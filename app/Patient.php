<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
	protected $primaryKey = 'patientid';

    public function sponsor(){
        return $this->hasMany('App\Sponsor', 'userid', 'userid');
    }
    public function userName(){
    	return $this->hasOne('App\User','id','userid');
    }
    public function donation(){
    	return $this->hasMany('App\Donation', 'id');
    }
    public function picture(){
    	return $this->hasMany('App\Picture', 'patientid', 'id');
    }




    public function stories(){
        return $this->hasMany('App\Stories', 'patientid', 'patientid');
    }
    

}
